/*
	Miniactivity
		create a User model with the following properties
			firstname - string, required
			lastname - string, required
			email - string, required
			password - string, required
			isAdmin - boolean false default
			mobileNo - string, required
			enrollments: array of objects
				- courseId - string, required
				- enrolledOn - date, new Date default
				- status - string, default: "Enrolled"
*/

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName:{
		type: String,
		required: [true, "Last name is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	enrollments: [
		{
			courseId:{
				type: String,
				required: [true, "Course id is required"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);
