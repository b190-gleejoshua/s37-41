const Course = require("../models/Course.js");
const auth = require("../auth.js");

// creates new course


module.exports.addCourse = (reqBody)=>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price

	})
	return newCourse.save().then((course, error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}


module.exports.getAllCourses = ()=>{
	return Course.find({ }).then(result => {
		return result;
	});
};

// retreive all active courses

module.exports.getAllActive = ()=>{
	return Course.find({isActive: true}).then(result =>{
		return result
		
	})
};

// route for retrieving a specific course

module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
};



// update a course

module.exports.updateCourse = (reqParams, reqBody) =>{
	let updateCourse = {
		name:reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - its purpose os to find a specific database (first Parmameter) and update it using the information from the request body (second Parameter)
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((result, error)=>{
		if (error){
			return false;
		}else{
			return true;
		}


	})

	
}

module.exports.archiveCourse = (reqParams, reqBody) =>{
	let updateActiveField ={
		isActive: false
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((result, error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}
